import socket
import struct
import random
from enum import Enum


class Directions(Enum):
    UP, DOWN, LEFT, RIGHT = 'UP', 'DOWN', 'LEFT', 'RIGHT'


class Node:
    """A node class for A* Pathfinding"""

    def __init__(self, parent=None, position=None):
        self.parent = parent
        self.position = position

        self.g = 0
        self.h = 0
        self.f = 0

    def __eq__(self, other):
        return self.position == other.position


class AI:
    # def fucking_start_node(self, bot_id, board):
    #     startnode = (0, 0)
    #     for i in range(enumerate(board)):
    #         for j in range(enumerate(board[i])):
    #             if board[i][j] == '0':
    #                 startnode = (i, j)
    #     return startnode
    #
    # def choose_fucking_goal(self, bot_id, board):
    #     fruit_distance_dict = {}
    #     fruit_index_dict = {}
    #
    #     for i, value in enumerate(board):
    #         for j, _ in enumerate(board[i]):
    #             if (board[i][j] == '0'):
    #                 rowindex = i
    #                 columnindex = j
    #
    #     for i, _ in enumerate(board):
    #         for j, _ in enumerate(board[i]):
    #
    #             if board[i][j] != '.' or board[i][j] != '*':
    #                 distance = (rowindex - i) ** 2 + (columnindex - j) ** 2
    #
    #                 if board[i][j] in fruit_distance_dict.keys():
    #
    #                     if distance < fruit_distance_dict[board[i][j]]:
    #                         fruit_distance_dict[board[i][j]] = distance
    #                         fruit_index_dict[board[i][j]] = (i, j)
    #                 else:
    #                     fruit_distance_dict[board[i][j]] = distance
    #                     fruit_index_dict[board[i][j]] = (i, j)
    #
    #     for fruit in fruit_distance_dict:
    #         minim = 10000
    #         if fruit_distance_dict[fruit] <= minim:
    #             minim = fruit_distance_dict[fruit]
    #             bestfruit = fruit
    #     return fruit_index_dict[bestfruit]

    def __init__(self, bot_id, bot_count, board_size):
        self.__bot_id, self.__bot_count, self.__board_size = bot_id, bot_count, board_size

    def do_turn(self, fruit_counter_array ,board, bot_fruits):
        def fucking_start_node(self, bot_id, board):
            startnode = (0, 0)
            for i,_ in  enumerate(board):
                for j , _ in enumerate(board[i]):
                    if board[i][j] == '0':
                        startnode = (i, j)
            return startnode

        def find_direction(first, second):
            if tuple(x - y for x, y in zip(first, second)) == (0, 1):
                return "LEFT"
            if tuple(x - y for x, y in zip(first, second)) == (0, -1):
                return "RIGHT"
            if tuple(x - y for x, y in zip(first, second)) == (1, 0):
                return "UP"
            if tuple(x - y for x, y in zip(first, second)) == (-1, 0):
                return "DOWN"

        def choose_fucking_goal(self, bot_id, array, board):
            fruit_distance_dict = {}
            fruit_index_dict = {}

            for i, _ in enumerate(board):
                for j, _ in enumerate(board[i]):
                    if board[i][j] == '0':
                        row_index = i
                        column_index = j

            for i, _ in enumerate(board):
                for j, _ in enumerate(board[i]):

                    if board[i][j] != '.' and board[i][j] != '*' and board[i][j] !='0' and board[i][j] !='1':
                        distance = (row_index - i) ** 2 + (column_index - j) ** 2

                        if board[i][j] in fruit_distance_dict.keys():

                            if distance < fruit_distance_dict[board[i][j]]:
                                fruit_distance_dict[board[i][j]] = distance
                                fruit_index_dict[board[i][j]] = (i, j)
                        else:
                            fruit_distance_dict[board[i][j]] = distance
                            fruit_index_dict[board[i][j]] = (i, j)
                        print(fruit_distance_dict)

            minim = 10000
            temp = dict(fruit_distance_dict)
            for fruit in temp:
                if fruit == 'B':
                    if (array[1]+1)*2 > array[4]:
                        del fruit_distance_dict[fruit]
                        del fruit_index_dict[fruit]
                        continue

                if fruit == 'C' or fruit == 'W':
                    if array[2] + array[3] + 1 > array[0]:
                        del fruit_distance_dict[fruit]
                        del fruit_index_dict[fruit]
                        continue

                if fruit_distance_dict[fruit] <= minim:
                    minim = fruit_distance_dict[fruit]
                    best_fruit = fruit
           # print("({0} , {1}) , ({2} , {3})".format(fruit_index_dict[best_fruit][0],fruit_index_dict[best_fruit][1],row_index,column_index))
            if fruit_distance_dict[best_fruit] == 1:
                if best_fruit == 'A':
                    array[0] += 1
                if best_fruit == 'B':
                    array[1] += 1
                if best_fruit == 'C':
                    array[2] += 1
                if best_fruit == 'W':
                    array[3] += 1
                if best_fruit == 'O':
                    array[4] += 1
            print(array)
            print(fruit_distance_dict)
            return fruit_index_dict[best_fruit]

      # end of choose goal


        #matris morede estefade dar bazi
        board_matrix = list()
        for value in board:
            values = list()
            for each in value:
                values.append(each)
            board_matrix.append(values)
        #liste mive haye khorde shode
        eaten_fruit = list(fruits)

        points = 0
        for i in range(len(eaten_fruit)):
            if (eaten_fruit[i] == 'W'):
                w_counter = w_counter + 1
                points = points + 3
            if (eaten_fruit[i] == 'C'):
                c_counter = c_counter + 1
                points = points + 2
            if (eaten_fruit[i] == 'A'):
                a_counter = a_counter + 1
                points = points + 1
            if (eaten_fruit[i] == 'O'):
                o_counter = o_counter + 1
                points = points + 1
            if (eaten_fruit[i] == 'B'):
                b_counter = b_counter + 1
                points = points + 5


        print(board_matrix)
        sn = fucking_start_node(self, bot_id, board_matrix)
        en = choose_fucking_goal(self, bot_id, fruit_counter_array, board_matrix)
        print(sn, en, sep='*' * 10)
        path = astar(self, board_matrix, sn, en)
        print(path)
        print(find_direction(path[0], path[1]))
        print()

        return find_direction(path[0], path[1])
        # write your bot's logic here

        # return random.choice(list(Directions))


def astar(self, maze, start, end):
    """Returns a list of tuples as a path from the given start to the given end in the given maze"""

    # Create start and end node
    start_node = Node(None, start)
    start_node.g = start_node.h = start_node.f = 0
    end_node = Node(None, end)
    end_node.g = end_node.h = end_node.f = 0
    # Initialize both open and closed list
    open_list = []
    closed_list = []

    # Add the start node
    open_list.append(start_node)

    # Loop until you find the end
    while len(open_list) > 0:
        # Get the current node
        current_node = open_list[0]
        current_index = 0
        for index, item in enumerate(open_list):
            if item.f < current_node.f:
                current_node = item
                current_index = index

        # Pop current off open list, add to closed list
        test = open_list.pop(current_index)
        closed_list.append(current_node)

        # Found the goal
        if current_node.position == end:
            path = []
            current = current_node
            while current is not None:
                path.append(current.position)
                current = current.parent
            return path[::-1]  # Return reversed path

        # Generate children
        children = []
        for new_position in [(0, -1), (0, 1), (-1, 0), (1, 0)]:  # Adjacent squares

            # Get node position
            node_position = (current_node.position[0] + new_position[0], current_node.position[1] + new_position[1])

            # Make sure within range
            if node_position[0] > (len(maze) - 1) or node_position[0] < 0 or node_position[1] > (
                    len(maze[len(maze) - 1]) - 1) or node_position[1] < 0:
                continue

            # Make sure walkable terrain
            if maze[node_position[0]][node_position[1]] == '*':
                # print (maze[node_position[0]][node_position[1]])
                continue

            # Create new node
            new_node = Node(current_node, node_position)

            # Append
            children.append(new_node)

        # Loop through children
        for child in children:

            # Child is on the closed list
            if child in closed_list:
                continue

            # Create the f, g, and h values
            child.g = current_node.g + 1
            child.h = ((child.position[0] - end_node.position[0]) ** 2) + (
                    (child.position[1] - end_node.position[1]) ** 2)
            child.f = child.g + child.h

            # Child is already in the open list
            for open_node in open_list:
                if child == open_node and child.g > open_node.g:
                    continue

            # Add the child to the open list
            open_list.append(child)


def read_utf(sock: socket.socket):
    length = struct.unpack('>H', s.recv(2))[0]
    return sock.recv(length).decode('utf-8')


def write_utf(sock: socket.socket, msg: str):
    sock.send(struct.pack('>H', len(msg)))
    sock.send(msg.encode('utf-8'))


if __name__ == '__main__':
    w_counter = 0
    c_counter = 0
    a_counter = 0
    o_counter = 0
    b_counter = 0
    fruit_counter_array = [a_counter, b_counter, c_counter, w_counter, o_counter]
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(('127.0.0.1', 9898))
    init_data = read_utf(s)
    bot_id, bot_count, board_size = map(int, init_data.split(','))
    ai = AI(bot_id, bot_count, board_size)
    while True:
        board_str = read_utf(s)
        board = [board_str[i * board_size:(i + 1) * board_size] for i in range(board_size)]
        fruits = [read_utf(s) for _ in range(bot_count)]

        write_utf(s, ai.do_turn(fruit_counter_array,board, fruits))
